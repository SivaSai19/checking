package com.OneToOne.demo;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringOneToOneApplication implements CommandLineRunner{

	@Autowired
	EmployeeRepository employeeRepository;
	
	@Autowired
	OrganizationRepository organizationRepository;
	
	public static void main(String[] args) {
		SpringApplication.run(SpringOneToOneApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		// TODO Auto-generated method stub
	//saveData();
	showData();
	}


	/*public void saveData() {
	
		Organization org = new Organization("mheights",new Employee("harsha"));
		
		organizationRepository.save(org);
	
	}*/
	public void showData() {
		List<Organization> org = organizationRepository.findAll();
		
		System.out.println("Retrieving orgnization table");
		
		org.forEach(System.out::println);
	}
	
}

