package com.OneToOne.demo;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name= "organization")
public class Organization {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer organization_id;
	
	private String organization_Loc;
	
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "employee_id")
    private Employee employee;

	public Organization() {
		super();
	}

	public Organization(String organization_Loc) {
		this.organization_Loc = organization_Loc;
	}

	public Organization(String organization_Loc, Employee employee) {
		this.organization_Loc = organization_Loc;
		this.employee = employee;
	}

	public Integer getOrganization_id() {
		return organization_id;
	}

	public void setOrganization_id(Integer organization_id) {
		this.organization_id = organization_id;
	}

	public String getOrganization_Loc() {
		return organization_Loc;
	}

	public void setOrganization_Loc(String organization_Loc) {
		this.organization_Loc = organization_Loc;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}
    
	
}
