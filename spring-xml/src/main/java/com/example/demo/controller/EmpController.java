package com.example.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.demo.model.Emp;

@Controller

public class EmpController {
	
	
	@RequestMapping(value ="/getxml")
	public @ResponseBody Emp value(@RequestParam String name) {
		
		
		Emp e = new Emp();
		e.setId(1);
		e.setName(name);
		
		return e;
	}

}
